﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public float timeBeforeFall;
    public HexCell currentCell;
    bool isFalling = false;
    float x0;
    float y0;
    float z0;
    float x1;
    float y1;
    float z1;
    float x;
    float y;
    float z;
    float speedX;
    float speedY;
    float speedZ;
    float t;

    public void Fall()
    {
        x0 = transform.position.x;
        y0 = transform.position.y;
        z0 = transform.position.z;

        x1 = currentCell.transform.position.x;
        y1 = currentCell.transform.position.y + 4f + currentCell.height.y;
        z1 = currentCell.transform.position.z;

        x = x0;
        y = y0;
        z = z0;

        speedX = (x1 - x0) / timeBeforeFall;
        speedY = (y1 - y0) / timeBeforeFall;
        speedZ = (z1 - z0) / timeBeforeFall;

        t = 0;
        isFalling = true;
    }

    void FixedUpdate()
    {
        if (isFalling && (t < timeBeforeFall - Time.deltaTime))
        {
            t += Time.deltaTime;
            x += Time.deltaTime * speedX;
            y += Time.deltaTime * speedY;
            z += Time.deltaTime * speedZ;
            transform.position = new Vector3(x, y, z);
            if (t >= timeBeforeFall - Time.deltaTime)
            {
                CreateLavaCells();
            }
        }
        if (currentCell == null)
        {
            Destroy(gameObject);
        }
    }
    
    void CreateLavaCells()
    {
    }
}
