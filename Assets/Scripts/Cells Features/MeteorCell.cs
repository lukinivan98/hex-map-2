﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class MeteorCell : HexCell
{

    public float timeBetweenSpawns = 5f;
    public float spawnDistance = 50f;
    public Vector3 spawnHeight = new Vector3(0, 3000f, 0);

    public Meteor meteorPrefab;

    void SpawnMeteor(HexCell cell)
    {
        Meteor spawn = Instantiate<Meteor>(meteorPrefab);
        spawn.gameObject.transform.SetParent(transform);
        spawn.currentCell = cell;
        spawn.transform.localPosition = (cell.transform.localPosition + Random.onUnitSphere * spawnDistance + spawnHeight);
        spawn.Fall();
    }
}