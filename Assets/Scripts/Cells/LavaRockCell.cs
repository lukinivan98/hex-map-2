﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class LavaRockCell : HexCell
{
    public float timeBeforeFall = 1f;
    bool playerOnCell = false;
    LavaRock rock;

    public override void Triangulate()
    {
        float step = Random.Range(0, gameObject.GetComponentInParent<HexGrid>().MaxStep);
        height = new Vector3(0, gameObject.GetComponentInParent<HexGrid>().h + step, 0);
        Vector3 center = gameObject.transform.localPosition;
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center, center + HexMetrics.corners[i + 1], center + HexMetrics.corners[i]);
        }
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + height - new Vector3(0, 5f, 0), center + HexMetrics.corners[i] + height, center + HexMetrics.corners[i + 1] + height);
        }
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i] + height);
        }

        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1]);
        }
        for (int i = 0; i < coliders.Length; i++)
        {
            coliders[i].Initialise(gameObject.transform.localPosition + height / 2f, new Vector3(HexMetrics.innerRadius * 2, height.y, HexMetrics.outerRadius));
        }
        hexMesh.vertices = this.vertices.ToArray();
        hexMesh.triangles = this.triangles.ToArray();
        hexMesh.RecalculateNormals();
    }

    private void Start()
    {
        rock = gameObject.GetComponentInChildren<LavaRock>();
    }

    public override void PlayerOn()
    {
        playerOnCell = true;
        StartCoroutine(DestroyRock());
    }

    public override void PlayerOff()
    {
        playerOnCell = false;
    }

    public IEnumerator DestroyRock()
    {
        WaitForSeconds wait = new WaitForSeconds(timeBeforeFall);
        yield return wait;
        Destroy(rock.gameObject);
        if (playerOnCell)
        {
            GameManager.playerAlive = false;
        }
    }
}