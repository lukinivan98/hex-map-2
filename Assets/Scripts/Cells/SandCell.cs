﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SandCell : HexCell
{
    public float timeBeforeFall = 1f;
    bool playerOnCell = false;
    float fallingHeight;

    public override void PlayerOn()
    {
        playerOnCell = true;
        StartCoroutine(Crush());
    }

    public override void PlayerOff()
    {
        playerOnCell = false;
    }

    public IEnumerator Crush()
    {
        WaitForSeconds wait = new WaitForSeconds(timeBeforeFall);
        yield return wait;
        fallingHeight = 1f;
    }

    private void Update()
    {
        if(fallingHeight > 0)
        {
            fallingHeight -= Time.deltaTime;
            transform.position -= new Vector3(0,fallingHeight,0);
            if (playerOnCell && fallingHeight<0.7f)
            {
                GameManager.playerAlive = false;
            }
        }
    }
}