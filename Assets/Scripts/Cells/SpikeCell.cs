﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SpikeCell : HexCell
{
    Spike spike;
    public float timeBeforeSpike = 1f;
    public bool spikeUp;
    private float currentTime;
    bool playerOnCell = false;

    private void Start()
    {
        spike = gameObject.GetComponentInChildren<Spike>();
        int a = Random.Range(0,2);
        if (a == 1)
        {
            spikeUp = true;
        }
        else
        {
            spikeUp = false;
        }
        currentTime = GameManager.timeCount%timeBeforeSpike;
    }

    public override void PlayerOn()
    {
        playerOnCell = true;
        if (playerOnCell && spikeUp)
        {
            GameManager.playerAlive = false;
        }
    }

    public override void PlayerOff()
    {
        playerOnCell = false;
    }
    

    private void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime>= timeBeforeSpike && !spikeUp)
        {
            spikeUp = true;
            spike.gameObject.SetActive(spikeUp);
            currentTime = 0f;
        }
        else if (currentTime >= timeBeforeSpike && spikeUp)
        {
            spikeUp = false;
            spike.gameObject.SetActive(spikeUp);
            currentTime = 0f;
        }
        if (playerOnCell && spikeUp)
        {
            GameManager.playerAlive = false;
        }
    }
}