﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexGrid : MonoBehaviour
{
    public int width = 13;
    public int height = 13;
    public float h;
    public int MaxStep;
    public float generationStepDelay;

    public HexCell[] cellPrefabs;

    public List<HexCell> cells;

    public float[] perlin;

    private Vector2 startPoint;
    public float size = 0.1f;

    public float power = 100f;

    float PerlinNoise(int X, int Z)
    {
        float xCoord = startPoint.x + X * size;
        float zCoord = startPoint.y + Z * size;
        return (Mathf.PerlinNoise(xCoord, zCoord) - 0.5f) * power;
    }

    public void Generate()
    {
        startPoint = new Vector2(Random.Range(0, 1000f), Random.Range(0, 1000f));
        cells = new List<HexCell>();
        CreateCellLine(0, 0, 1);
        for (int i = 1; i < height; i++)
        {
            CreateCellLine(cells[(i-1)*width].coordinates.X, i, i%2);
        }
        for (int i = 0; i < width * (height - 1); i++)
        {
            if (i % (2 * width) == 0)
            {
                cells[i].leftCell = null;
            }
            else
            {
                cells[i].leftCell = cells[i + width - ((i / width)+1) % 2];
            }
            if (i % (2 * width) == 2 * width - 1)
            {
                cells[i].rightCell = null;
            }
            else
            {
                cells[i].rightCell = cells[i + width + (1 - ((i/width)+1)%2)];
            }
        }
    }

    void CreateCell(int x, int z)
    {
        Vector3 position;
        position.x = (x + z * 0.5f) * (HexMetrics.innerRadius * 2f);
        position.y = PerlinNoise(x,z);
        position.z = z * (HexMetrics.outerRadius * 1.5f);
        int a;
        if (position.y <= -10f)
        {
            position.y -= 1f;
            a = Random.Range(8, 10);
        }
        else
        {
            a = Random.Range(0, 8);
        }
        HexCell cell = Instantiate(cellPrefabs[a]);
        cells.Add(cell);
        cell.transform.SetParent(transform, false);
        cell.transform.localPosition = position;
        cell.Triangulate();
        cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);
        cell.typeOfCell = a;
    }

    void CreateCellLine(int x, int z, int direction)
    {
        for (int i = x - (1-direction); i < x + width - (1 - direction); i++)
        {
            CreateCell(i, z);
        }
    }

    void DestroyCellLine()
    {
        for (int i = 0; i < width; i++)
        {
            Destroy(cells[i].gameObject);
        }
        cells.RemoveRange(0, width);
    }

    void ConnectCellLine(int direction)
    {
        for (int i = width * (height - 2) + 1; i < width * (height - 1) - 1; i++)
        {
            cells[i].rightCell = cells[i + width + (1 - direction)];
            cells[i].leftCell = cells[i + width - direction];
        }
    }

    public void SpawnNewLine(int direction)
    {
        DestroyCellLine();
        CreateCellLine(cells[width * (height - 2)].coordinates.X, cells[width * (height - 2)].coordinates.Z + 1, direction);
        ConnectCellLine(direction);
    }

    public void SwitchCellToType<T>(HexCell cell, int a)
    {
        HexCell newCell = Instantiate(cellPrefabs[a]);
        cells[1] = newCell;
        newCell.transform.position = cell.transform.position;
        newCell.coordinates = cell.coordinates;
        newCell.rightCell = cell.rightCell;
        newCell.leftCell = cell.leftCell;
        cells.Add(newCell);
        newCell.transform.SetParent(transform, false);
        newCell.Triangulate();

        Destroy(cell.gameObject);
    }
}