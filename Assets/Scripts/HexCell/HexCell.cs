﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexCell : MonoBehaviour
{
    public HexCoordinates coordinates;
    public Vector3 height;

    public HexColider[] coliders;
    public Mesh hexMesh;
    public List<Vector3> vertices;
    public List<int> triangles;

    public HexCell leftCell;
    public HexCell rightCell;

    public int typeOfCell;

    void Awake()
    {
    //    coliders = gameObject.GetComponentsInChildren<HexColider>();
    //    GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
    //    hexMesh.name = "HexMesh";
    //    vertices = new List<Vector3>();
    //    triangles = new List<int>();
    }

    public virtual void PlayerOn()
    {

    }

    public virtual void PlayerOff()
    {

    }


    public virtual void Triangulate()
    {
        //float step = Random.Range(0, gameObject.GetComponentInParent<HexGrid>().MaxStep);
        //height = new Vector3(0, gameObject.GetComponentInParent<HexGrid>().h + step, 0);
        //Vector3 center = gameObject.transform.localPosition;
        //for (int i = 0; i < 6; i++)
        //{
        //    AddTriangle(center, center + HexMetrics.corners[i + 1], center + HexMetrics.corners[i]);
        //}
        //for (int i = 0; i < 6; i++)
        //{
        //    AddTriangle(center + height, center + HexMetrics.corners[i] + height, center + HexMetrics.corners[i + 1] + height);
        //}
        //for (int i = 0; i < 6; i++)
        //{
        //    AddTriangle(center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i] + height);
        //}

        //for (int i = 0; i < 6; i++)
        //{
        //    AddTriangle(center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1]);
        //}
        //for (int i = 0; i < coliders.Length; i++)
        //{
        //    coliders[i].Initialise(gameObject.transform.localPosition + height / 2f, new Vector3(HexMetrics.innerRadius * 2, height.y, HexMetrics.outerRadius));
        //}
        //hexMesh.vertices = this.vertices.ToArray();
        //hexMesh.triangles = this.triangles.ToArray();
        //hexMesh.RecalculateNormals();
        //transform.localScale = new Vector3(17.32050808f, 17.32050808f * 5f, 17.32050808f);
    }

    public void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }
}