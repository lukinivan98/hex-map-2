﻿using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
public class HexColider : MonoBehaviour
{
    public void Initialise(Vector3 center, Vector3 size)
    {
        gameObject.transform.position = center;
        GetComponent<BoxCollider>().size = size;
    }
}
