﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public HexGrid fieldPrefab;
    private HexGrid fieldInstance;
    public Player playerPrefab;
    private Player playerInstance;
    public static bool playerAlive;
    //public MeteorSpawner meteorSpawnerPrefab;
   // private MeteorSpawner meteorSpawner;
    public static float timeCount;
    public int spawnPosition;

    private void Update()
    {
        timeCount += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space) || !playerAlive)
        {
            RestartGame();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void Start()
    {
        BeginGame();
    }

    private void BeginGame()
    {
        Camera.main.clearFlags = CameraClearFlags.Skybox;
        Camera.main.rect = new Rect(0f, 0f, 1f, 1f);
        fieldInstance = Instantiate(fieldPrefab) as HexGrid;
        fieldInstance.Generate();

        playerInstance = Instantiate(playerPrefab) as Player;
        playerAlive = true;
        playerInstance.Initialize(fieldInstance.cells[spawnPosition]);
        
        //meteorSpawner = Instantiate(meteorSpawnerPrefab) as MeteorSpawner;
        //meteorSpawner.Initialize(fieldInstance);
        Camera.main.clearFlags = CameraClearFlags.Depth;
        Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);
    }

    private void RestartGame()
    {
        StopAllCoroutines();
        Destroy(fieldInstance.gameObject);
        //Destroy(meteorSpawner.gameObject);
        if (playerInstance != null)
        {
            Destroy(playerInstance.gameObject);
        }
        BeginGame();
    }
    
}